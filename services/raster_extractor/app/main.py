import hashlib
import os

from fastapi import FastAPI
from app.raster_meta_extract import extract_from_tif_file, extract_metadata_from_vrt, get_vrt_files, list_tif_files
from pymongo import MongoClient
from google.cloud import storage
from pathlib import Path
from turfpy.measurement import bbox_polygon, bbox
from geojson import Point, Feature


def get_database() -> MongoClient:
    username = os.environ.get("MONGO_USERNAME")
    password = os.environ.get("MONGO_PASSWORD")
    database = os.environ.get("MONGO_DATABASE")
    CONNECTION_STRING = f"mongodb+srv://{username}:{password}@catalog.vs3iwdn.mongodb.net/{database}"
    client = MongoClient(CONNECTION_STRING)
    return client['filebased']["cloudbucket"]

app = FastAPI()

@app.get("/list-vrt-files")
def list_vrt_file(objectId: str, bucketId: str):
    vrt_files = get_vrt_files(bucketId, objectId)
    return [to_file_entry(f) for f in vrt_files]

@app.post("/metadata")
def extract_metadata(objectId: str, bucketId: str):
    client = storage.Client()
    bucket = client.get_bucket(bucketId)
    if objectId.endswith(".vrt"):
        file_path = f"/vsigs_streaming/{bucketId}/{objectId}"
        json_metadata = extract_metadata_from_vrt(file_path)
        file_blob = bucket.get_blob(objectId)
        files = list_tif_files(file_blob)
        prefix = os.path.dirname(objectId)
        all_files = [file_blob.public_url]
        for f in files:
            all_files.append(bucket.get_blob(f"{prefix}/{f}").public_url)
        json_metadata["files"] = all_files
    else:
        # determine if tif file is referenced in a vrt
        # if the tif is referenced by a vrt, then we're done
        # if the tif is not referenced by a vrt, then create a vrt file and extract metadata from it
        vrt_files = get_vrt_files(bucketId, objectId)
        if len(vrt_files) > 0:
            return
        file_path = f"/vsigs_streaming/{bucketId}/{objectId}"
        json_metadata = extract_from_tif_file(file_path)
        json_metadata["files"] = [bucket.get_blob(objectId).public_url]

    file_blob = bucket.get_blob(objectId)
    json_metadata["id"] = hashlib.md5(bytes(file_blob.public_url, 'utf-8')).hexdigest()
    json_metadata["_id"] = json_metadata["id"]
    json_metadata["url"] = file_blob.public_url
    title = os.path.splitext(Path(objectId).name)[0]
    json_metadata["title"] = title

    if "spatial_coverage" in json_metadata:
        coverage = json_metadata["spatial_coverage"]
        coverage_value = coverage["value"]

        if coverage["type"] == "box":
            bbox = [float(coverage_value["northlimit"]), float(coverage_value["southlimit"]), float(coverage_value["eastlimit"]), float(coverage_value["westlimit"])]
            json_metadata["spatial_coverage"] = bbox_polygon(bbox)
        elif coverage["type"] == "point":
            point = Feature(geometry=Point([float(coverage_value["east"]), float(coverage_value["north"])]))
            json_metadata["spatial_coverage"] = point

    bucket_checksum = hashlib.md5(bytes(bucketId, 'utf-8')).hexdigest()
    json_metadata["relations"] = [{"type": "isPartOf", "value": bucket_checksum}]

    collection = get_database()

    collection.update_one({"_id": json_metadata["_id"]}, {"$set": json_metadata}, upsert=True)

    return json_metadata["id"]

def to_file_entry(f_blob):
    return {"path": f_blob.public_url, "size": f_blob.size, "checksum": f_blob.md5_hash, "mime_type": f_blob.content_type}
import hashlib
import os
from fastapi import FastAPI
from app.feature_extractor import extract_metadata_and_files, get_all_related_shp_files
from pymongo import MongoClient
from turfpy.measurement import bbox_polygon
from geojson import Point, Feature


def get_database() -> MongoClient:
    username = os.environ.get("MONGO_USERNAME")
    password = os.environ.get("MONGO_PASSWORD")
    database = os.environ.get("MONGO_DATABASE")
    CONNECTION_STRING = f"mongodb+srv://{username}:{password}@catalog.vs3iwdn.mongodb.net/{database}"
    client = MongoClient(CONNECTION_STRING)
    return client['filebased']["cloudbucket"]

app = FastAPI()

@app.get("/list-related")
def list_related_files(objectId: str, bucketId: str):
    shp_files, _, _ = get_all_related_shp_files(bucketId, objectId)
    return [to_file_entry(f) for f in shp_files]

@app.post("/metadata")
def extract_metadata_endpoint(objectId: str, bucketId: str):
    json_metadata = extract_metadata_and_files(bucketId, objectId)
    json_metadata["_id"] = json_metadata["id"]

    coverage = json_metadata["coverage"]
    coverage_value = coverage["value"]
    
    if coverage["type"] == "box":
        bbox = [float(coverage_value["northlimit"]), float(coverage_value["southlimit"]), float(coverage_value["eastlimit"]), float(coverage_value["westlimit"])]
        json_metadata["spatial_coverage"] = bbox_polygon(bbox)
        del json_metadata["coverage"]
    elif coverage["type"] == "point":
        point = Feature(geometry=Point([float(coverage_value["east"]), float(coverage_value["north"])]))
        json_metadata["spatial_coverage"] = point
        del json_metadata["coverage"]
    
    bucket_checksum = hashlib.md5(bytes(bucketId, 'utf-8')).hexdigest()
    json_metadata["relations"] = [{"type": "isPartOf", "value": bucket_checksum}]

    collection = get_database()
    collection.update_one({"_id": json_metadata["_id"]}, {"$set": json_metadata}, upsert=True)
    return json_metadata["id"]

def to_file_entry(f_blob):
    return {"path": f_blob.public_url, "size": f_blob.size, "checksum": f_blob.md5_hash, "mime_type": f_blob.content_type}
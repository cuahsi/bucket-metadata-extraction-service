variable "mongodb-apikey" {
  type = string
  sensitive = true
}
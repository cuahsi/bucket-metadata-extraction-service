provider "google" {
  project = "thredds"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_secret_manager_secret" "secret-basic" {
  project       = "thredds"
  secret_id     = "mongodb-apikey"

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "secret-version-basic" {
  secret = google_secret_manager_secret.secret-basic.id

  secret_data = var.mongodb-apikey
}

resource "google_cloud_run_service" "extract_raster_service" {
  project  = "thredds"
  name     = "extract-raster-service"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "us-central1-docker.pkg.dev/thredds/metadata-extraction/raster-extractor:0.2.3"
        env {
          name  = "GS_OAUTH2_PRIVATE_KEY"
          value = google_service_account_key.secret_accessor_key.private_key
        }
        env {
          name  = "GS_OAUTH2_CLIENT_EMAIL"
          value = google_service_account.secret_accessor_sa.email
        }
        env {
          name = "MONGO_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "mongodb-apikey"
            }
          }
        }
        env {
          name  = "MONGO_USERNAME"
          value = "hydroshare"
        }
        env {
          name  = "MONGO_DATABASE"
          value = "test"
        }
      }
      service_account_name = google_service_account.secret_accessor_sa.email
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_binding" "extract_raster_service_sa_binding" {
  project  = "thredds"
  location = "us-central1"

  service = "extract-raster-service"
  role    = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}

resource "google_service_account" "secret_accessor_sa" {
  account_id   = "secret-accessor"
  display_name = "Secret Accessor"
}

resource "google_service_account_key" "secret_accessor_key" {
  service_account_id = "${google_service_account.secret_accessor_sa.name}"
}

resource "google_project_iam_member" "secret_accessor_binding" {
  project = "thredds"
  role    = "roles/secretmanager.secretAccessor"
  member = "serviceAccount:${google_service_account.secret_accessor_sa.email}"
}

resource "google_project_iam_member" "bucket_get_binding" {
  project = "thredds"
  role    = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.secret_accessor_sa.email}"
}

resource "google_cloud_run_service" "extract_feature_service" {
  project  = "thredds"
  name     = "extract-feature-service"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "us-central1-docker.pkg.dev/thredds/metadata-extraction/feature-extractor:0.2.3"
        env {
          name  = "GS_OAUTH2_PRIVATE_KEY"
          value = google_service_account_key.secret_accessor_key.private_key
        }
        env {
          name  = "GS_OAUTH2_CLIENT_EMAIL"
          value = google_service_account.secret_accessor_sa.email
        }
        env {
          name = "MONGO_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "mongodb-apikey"
            }
          }
        }
        env {
          name  = "MONGO_USERNAME"
          value = "hydroshare"
        }
        env {
          name  = "MONGO_DATABASE"
          value = "test"
        }
      }
      service_account_name = google_service_account.secret_accessor_sa.email
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_binding" "extract_feature_service_sa_binding" {
  project  = "thredds"
  location = "us-central1"

  service = "extract-feature-service"
  role    = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}

resource "google_cloud_run_service" "bucket_file_listing_service" {
  project  = "thredds"
  name     = "bucket-file-listing-service"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "us-central1-docker.pkg.dev/thredds/metadata-extraction/bucket-file-listing:0.1.1"
        env {
          name = "MONGO_PASSWORD"
          value_from {
            secret_key_ref {
              key  = "latest"
              name = "mongodb-apikey"
            }
          }
        }
        env {
          name  = "MONGO_USERNAME"
          value = "hydroshare"
        }
        env {
          name  = "MONGO_DATABASE"
          value = "test"
        }
      }
      service_account_name = google_service_account.secret_accessor_sa.email
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_binding" "bucket_file_listing_service_sa_binding" {
  project  = "thredds"
  location = "us-central1"

  service = "bucket-file-listing-service"
  role    = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}
